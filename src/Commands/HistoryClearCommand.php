<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\CommandHistory;

class HistoryClearCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'history:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear saved history';


    /**
     * @var object
     */
    protected $commandHistory;


    public function __construct()
    {
        $this->commandHistory = new CommandHistory();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $clear = $this->commandHistory->clearAll();
        $this->comment('History cleared!');
    }
}
