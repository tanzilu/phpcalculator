<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\CommandHistory;

class HistoryListCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'history:list {commands?* : filter the history by commands}{--D|driver=database : Driver for storage connection} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show calculator history';


    /**
     * @var object
     */
    protected $commandHistory;


    public function __construct()
    {
        $this->commandHistory = new CommandHistory();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $commands = $this->argument('commands');
        $driver = $this->option('driver');
        $data = $this->commandHistory->findAll($driver, $commands);
        if (!empty($data)) {
            $this->createTable($data);
        } else {
            $this->comment('History is empty.');
        }
    }

    /**
     * @param array|collection $data
     */
    private function createTable($data)
    {
        $tableContent = [];
        $i = 0;
        foreach ($data as $key => $row) {
            $i++;
            $tableContent[] = [
                'no' => $i,
                'command' => ucfirst($row['command']),
                'history_description' => $row['description'],
                'Result' => $row['result'],
                'Output' => $row['description'] . ' = ' . $row['result'],
                'Time' => $row['created_at']
            ];
        }
        $headers = ['No', 'Command', 'Description', 'Result', 'Output', 'Time'];
        $this->table($headers, $tableContent);
    }
}
