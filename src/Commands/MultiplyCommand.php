<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\CommandHistory;


class MultiplyCommand extends Command
{

    use \Jakmall\Recruitment\Calculator\Traits\CommandTrait;

    /**
     * @var string
     */
    protected $name = 'multiply';

    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var object
     */
    protected $commandHistory;

    public function __construct()
    {

        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {numbers* : The numbers to be %s}',
            $commandVerb,
            $this->getCommandPassiveVerb()
        );
        $this->description = sprintf('%s all given Numbers', ucfirst($commandVerb));
        parent::__construct($this);
    }

    protected function getCommandVerb(): string
    {
        return 'multiply';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'multiplied';
    }

    public function handle(): void
    {
        $numbers = $this->getInput();
        $description = $this->generateCalculationDescription($numbers, $this->getOperator());
        $result = $this->calculateAll($numbers, $this->getCommandVerb());
        $this->commandHistory = new CommandHistory();

        $this->commandHistory->log([
            'command' => $this->getCommandVerb(),
            'description' => $description,
            'result' => $result
        ]);

        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function getInput(): array
    {
        return $this->argument('numbers');
    }

    protected function getOperator(): string
    {
        return '*';
    }
}
