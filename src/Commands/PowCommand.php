<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\CommandHistory;


class PowCommand extends Command
{

    use \Jakmall\Recruitment\Calculator\Traits\CommandTrait;

    /**
     * @var string
     */
    protected $name = 'pow';

    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var object
     */
    protected $commandHistory;

    public function __construct()
    {
        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {base : The base number},
            {exp : The exponent number} %s',
            $commandVerb,
            $this->getCommandPassiveVerb()
        );
        $this->description = sprintf('%s the given Numbers', ucfirst($commandVerb));
        parent::__construct($this);
    }

    protected function getCommandVerb(): string
    {
        return 'pow';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'powed';
    }

    public function handle(): void
    {
        $numbers = $this->getInput();
        $description = $this->generateCalculationDescription($numbers, $this->getOperator());
        $result = $this->calculateAll($numbers, $this->getCommandVerb());
        $this->commandHistory = new CommandHistory();

        $this->commandHistory->log([
            'command' => $this->getCommandVerb(),
            'description' => $description,
            'result' => $result
        ]);

        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function getInput(): array
    {
        $arguments = ['base', 'exp'];
        foreach ($arguments as $argument) {
            $input[] = $this->argument($argument);
        }
        return $input;
    }

    protected function getOperator(): string
    {
        return '^';
    }
}
