<?php

namespace Jakmall\Recruitment\Calculator\History;

use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\Models\HistoryDB;
use Jakmall\Recruitment\Calculator\Models\HistoryFile;

class CommandHistory implements CommandHistoryManagerInterface
{

    public function findAll($driver = 'database', $attributes = []): array
    {

        if ($driver === 'database')
            return HistoryDB::findByAttributes($attributes);
        else
            return HistoryFile::findByAttributes($attributes);
    }

    public function log($command): bool
    {
        try {
            $db = HistoryDB::create($command);
            HistoryFile::create($db);

            return true;
        } catch (HistoryDB $e) {
            return false;
        }
    }

    public function clearAll(): bool
    {
        return HistoryDB::truncate() && HistoryFile::truncate();
    }
}
