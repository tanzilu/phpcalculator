<?php

namespace Jakmall\Recruitment\Calculator\Models;

use illuminate\Support\Collection;

class File
{

    private $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function getHistory()
    {
        $checkFile = file_exists($this->path);

        if ($checkFile === false)
            file_put_contents($this->path, '[]');
        return json_decode(file_get_contents($this->path), TRUE);
    }

    public function getHistoryWithFilter(array $attr)
    {
        $data = json_decode(file_get_contents($this->path), TRUE);
        var_dump($attr);
        if ($attr === []) {
            return $this->getHistory();
        } else {
            $filterData = array_filter($data, function ($key) use ($attr) {
                foreach ($attr as $attr) {
                    if ($key['command'] == $attr) {
                        return true;
                    }
                }
            });
            return $filterData;
        }
    }

    public function saveHistory($data)
    {
        $content = $this->getHistory();
        array_push($content, $data);
        return file_put_contents($this->path, json_encode($content));
    }


    public function clearHistory()
    {
        if (file_exists($this->path)) {
            unlink($this->path);
        }
    }

    public static function data($path)
    {
        return (new self($path));
    }
}
