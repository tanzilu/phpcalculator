<?php

namespace Jakmall\Recruitment\Calculator\Models;

use Illuminate\Database\Eloquent\Model;


class HistoryDB extends Model
{
    const UPDATED_AT = null;

    protected $table = 'history';
    protected $fillable = ['id', 'command', 'description', 'result', 'created_at'];

    public static function findByAttributes(array $attr = [])
    {
        if (empty($attr))
            return self::get()->toArray();
        else
            return self::whereIn('command', $attr)->get()->toArray();
    }
}
