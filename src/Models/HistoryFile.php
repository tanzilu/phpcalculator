<?php

namespace Jakmall\Recruitment\Calculator\Models;

class HistoryFile
{

    static public $path = 'storage/history.json';

    public static function findByAttributes(array $attr = [])
    {
        return File::data(static::$path)->getHistoryWithFilter($attr);
    }

    public static function create($command)
    {
        return File::data(static::$path)->saveHistory($command);
    }

    public static function truncate()
    {
        return File::data(static::$path)->clearHistory();
    }
}
