<?php

namespace Jakmall\Recruitment\Calculator\Traits;


trait CommandTrait
{

    /**
     * @param array $input
     * @param string $operator
     *
     * @return string
     */
    public  function generateCalculationDescription(array $numbers, string $operator): string
    {
        $glue = sprintf(' %s ', $operator);
        return implode($glue, $numbers);
    }

    /**
     * @param array $input
     * @param string $command
     *
     * @return float|int
     */
    public function calculateAll(array $inputs, string $command)
    {
        $input = array_pop($inputs);

        if (count($inputs) <= 0) {
            return $input;
        }

        return $this->calculate($this->calculateAll($inputs, $command), $input, $command);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     * @param string $command
     *
     * @return int|float
     */
    protected function calculate($number1, $number2, $command)
    {
        switch ($command) {
            case 'add':
                return $number1 + $number2;
                break;
            case 'subtract':
                return $number1 - $number2;
                break;
            case 'multiply':
                return $number1 * $number2;
                break;
            case 'divide':
                return $number1 / $number2;
                break;
            case 'pow':
                return $number1 ** $number2;
                break;
            default:
                return 0;
        }
    }
}
